// This loads the environment variables from the .env file
require("dotenv-extended").load();

const builder = require("botbuilder");
const restify = require("restify");
const handlebars = require("handlebars");
const path = require("path");
const config = require("./config");
const fs = require("fs");
var nodemailer = require("nodemailer");
var sgTransport = require("nodemailer-sendgrid-transport");

// Setup Restify Server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
  console.log(`${server.name} listening to ${server.url}`);
});

// Create connector and listen for messages
const connector = new builder.ChatConnector({
  appId: process.env.MICROSOFT_APP_ID,
  appPassword: process.env.MICROSOFT_APP_PASSWORD
});

server.post("/api/messages", connector.listen());

// Default store: volatile in-memory store - Only for prototyping!
var inMemoryStorage = new builder.MemoryBotStorage();
var bot = new builder.UniversalBot(connector, function(session) {
  session.send(
    "Sorry, I did not understand '%s'. Type 'help' if you need assistance.",
    session.message.text
  );
}).set("storage", inMemoryStorage); // Register in memory storage

// You can provide your own model by specifing the 'LUIS_MODEL_URL' environment variable
// This Url can be obtained by uploading or creating your model from the LUIS portal: https://www.luis.ai/
const recognizer = new builder.LuisRecognizer(process.env.LUIS_MODEL_URL);
bot.recognizer(recognizer);

//Query object
const queryObject = {};

//Nodemailer
const options = {
  auth: {
    api_user: config.username,
    api_key: config.password
  }
};

const client = nodemailer.createTransport(sgTransport(options));

//Greetings
bot
  .dialog("Greetings", [
    (session, args, next) => {
      var assistance = new builder.Message(session)
        .text("Hello there! How can I assist you today? ✋🏻")
        .suggestedActions(
          builder.SuggestedActions.create(session, [
            builder.CardAction.imBack(session, "Posts", "Posts"),
            builder.CardAction.imBack(session, "Query", "Query"),
            builder.CardAction.imBack(session, "Subscribe", "Subscribe")
          ])
        );
      session.send(assistance);
    }
  ])
  .triggerAction({
    matches: "Greetings"
  });

//Subscribe

bot
  .dialog("Subscribe", [
    (session, args, next) => {
      session.send("Please enter your email ID to subscribe:");
      queryObject.query = args.intent.intent;
    }
  ])
  .triggerAction({
    matches: "Subscribe"
  });

//Contact

bot
  .dialog("Contact", [
    (session, args, next) => {
      // session.send("Please enter your query:");
      if (session.message && session.message.value) {
        function processSubmitAction(session, value) {
          var defaultErrorMessage = "Please enter your query:";
          if (!value.query || !value.emailID) {
            session.send(defaultErrorMessage);
            return false;
          } else {
            return true;
          }
        }
        // A Card's Submit Action obj was received
        if (processSubmitAction(session, session.message.value)) {
          next(session.message.value);
        }
        return;
      }
      // Display Welcome card with Hotels and Flights search options
      const contactQuery = {
        contentType: "application/vnd.microsoft.card.adaptive",
        content: {
          $schema: "http://adaptivecards.io/schemas/adaptive-card.json",
          type: "AdaptiveCard",
          version: "1.0",
          body: [
            {
              type: "ColumnSet",
              columns: [
                {
                  type: "Column",
                  width: 2,
                  items: [
                    {
                      type: "TextBlock",
                      text: "Please enter the following details!",
                      weight: "bolder",
                      size: "medium"
                    },
                    {
                      type: "TextBlock",
                      text: "Please enter your query:"
                    },
                    {
                      type: "Input.Text",
                      id: "query",
                      placeholder: "Please enter your query"
                    },
                    {
                      type: "TextBlock",
                      text: "Please enter your email:"
                    },
                    {
                      type: "Input.Text",
                      id: "emailID",
                      placeholder: "Please enter your email..."
                    }
                  ]
                }
              ]
            }
          ],
          actions: [
            {
              type: "Action.Submit",
              title: "Submit"
            }
          ]
        }
      };

      const contactCard = new builder.Message(session).addAttachment(
        contactQuery
      );
      session.send(contactCard);
    },
    (session, results) => {
      templateString1 = fs.readFileSync(
        path.join(__dirname, "mailer", "contactUser.handlebars"),
        "utf8"
      );
      fn1 = handlebars.compile(templateString1);
      const contactUser = {
        from: "query@maneeshavenigalla.com",
        to: results.emailID,
        subject: "Query - Contact",
        html: fn1()
      };
      templateString2 = fs.readFileSync(
        path.join(__dirname, "mailer", "contactMe.handlebars"),
        "utf8"
      );
      fn2 = handlebars.compile(templateString2);
      templateData2 = {
        message: results.query
      };
      const contactMe = {
        from: results.emailID,
        to: "venigallamaneesha24@gmail.com",
        subject: "Contact me",
        html: fn2(templateData2)
      };
      client.sendMail(contactUser, (err, info) => {
        if (err) {
          console.log("Error for user", err);
        }
        client.sendMail(contactMe, (err, info) => {
          if (err) {
            console.log("Error for me", err);
          }
          session.send(
            "Thanks for your query!! I will get back to you ASAP 🏃🏻‍!!"
          );
        });
      });
    }
  ])
  .triggerAction({
    matches: "Contact"
  });

//Mailer

bot
  .dialog("Mailer", [
    (session, args, next) => {
      if (queryObject.query === "Subscribe") {
        templateString1 = fs.readFileSync(
          path.join(__dirname, "mailer", "subscribeUser.handlebars"),
          "utf8"
        );
        fn1 = handlebars.compile(templateString1);

        const subscribeUser = {
          from: "subscribe@maneeshavenigalla.com",
          to: session.message.text,
          subject: "Thanks for subscribing ",
          html: fn1()
        };

        templateString2 = fs.readFileSync(
          path.join(__dirname, "mailer", "subscribeMe.handlebars"),
          "utf8"
        );
        fn2 = handlebars.compile(templateString2);

        templateData2 = {
          email: session.message.text
        };
        const subscribeMe = {
          from: session.message.text,
          to: "venigallamaneesha24@gmail.com",
          subject: "Subscribe me",
          html: fn2(templateData2)
        };

        client.sendMail(subscribeUser, (err, info) => {
          if (err) {
            console.log("Error from user mail", err);
          }

          client.sendMail(subscribeMe, (err, info) => {
            if (err) {
              console.log("Error to me", err);
            } else {
              session.send(
                "You have been subscribed!! Please check your spam/promotions folder 📂."
              );
            }
          });
        });
      } else {
      }
    }
  ])
  .triggerAction({
    matches: "Mailer"
  });

//End greetings
bot
  .dialog("EndGreetings", [
    (session, args, next) => {
      session.send("Thank you!! Have a nice day 🖤");
    }
  ])
  .triggerAction({
    matches: "EndGreetings"
  });
